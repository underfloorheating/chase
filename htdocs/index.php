<?php
$mailSent = NULL;
if(!empty($_POST)){

	$to = "info@chasetradeservices.co.uk";
	$subject = "An enquiry has been made through the website";

	$headers[] = 'From: info@chasetradeservices.co.uk';
	$headers[] = 'Reply-To: info@chasetradeservices.co.uk';
	$headers[] = 'X-Mailer: PHP/' . phpversion();
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=iso-8859-1';

	$message = '<html>
	<head>
		<title>Website enquiry</title>
	</head>
	<body>
		<table>
			<tr>
				<th>Name : </th>
				<td>' . $_POST['name'] . '</td>
			</tr>
			<tr>
				<th>Email : </th>
				<td>' . $_POST['email'] . '</td>
			</tr>
			<tr>
				<th>Telephone : </th>
				<td>' . $_POST['tel'] . '</td>
			</tr>
			<tr>
				<th>Message : </th>
				<td>' . $_POST['enquiry'] . '</td>
			</tr>
		</table>
	</body>
	</html>';

	if(mail($to, $subject, $message, implode("\r\n", $headers))) {
		$mailSent = true;
	}
}
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Chase Trade Services</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="initial-scale=1.0, width=device-width">

	<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />

</head>
<body id="top">

	<nav id="mobile-nav">
		<a href="#" id="mobile-nav-trigger">
			<img src="images/ellipse.jpg" />
		</a>
		<ul>
			<li><a href="#about-us">About us</a></li>
			<li><a href="#accolades-innovation">Accolades &amp; Innovation</a></li>
			<li><a href="#contact-us">Contact Us</a></li>
			<li><a href="#product-list">Download Product List</a></li>
		</ul>
	</nav>
	<header>
		<div class="container">
			<nav id="desktop-nav">
				<ul>
					<li><a href="#about-us">About us</a></li>
					<li><a href="#accolades-innovation">Accolades &amp; Innovation</a></li>
					<li><a href="#contact-us">Contact Us</a></li>
				</ul>
			</nav>
			<img src="images/logo.jpg" id="logo" />
			<a href="#product-list" class="product-list-link">
				<img src="images/product-list-link.png" target="_blank" />
			</a>
		</div>
	</header>

	<div id="main-container">
		<div id="top-section">
			<div class="container">
				<?php
				/**
				 * Output the email delivery response message
				 */
				if($mailSent !== NULL) {
					if($mailSent === false) {
						$class = 'error';
						$response = "<p>There was a problem sending your email, please try again.  If this issue persists, give us a call on 01268 772096.</p>";
					} else {
						$class = 'success';
						$response = "<p>Congratulations, your message is on it's way to our highly experienced team, one of whom will contact you shortly to discuss your requirements further.</p>";
					}
					?>
					<div id="email-response" class="<?php echo $class;?>">
						<?php echo $response;?>
					</div>
					<?php
				}
				?>
				<section>
					<div class="copy-container">
						<div class="title">
							<img src="images/titles/pet-food.png" />
						</div>
						<p>Over the last decade, Chase Trade Services has become an integral part of the pet food industry.</p>
						<p>We are able to offer a large range of products to our customers.</p>
					</div>
					<div class="image-container" id="polaroid-girl">
						<img src="images/polaroid-girl.png" />
					</div>
				</section>
				<section>
					<div class="image-container" id="polaroid-cows">
						<img src="images/polaroid-cows.png" />
					</div>
					<div class="copy-container">
						<div class="title">
							<img src="images/titles/animal-feed.png" />
						</div>
						<p>Chase Trade Services is one of the leading U.K. suppliers of amino acids, powder fats and milk products.  By keeping up to date with the latest industry developments and with an extensive supply network, we continue to be able to provide a first rate service to our customers.</p>
					</div>
				</section>
				<section id="product-list">
					<div class="products-wording">
						<div class="title">
							<img src="images/titles/product-list.png" />
						</div>
						<p>The current product list highlights some of the products that we can source.</p>
						<p>If there is any ingredient you would like us to source that is not included, please contact us and we will endeavour to support your needs.</p>
					</div>
					<div class="products-wrapper">
						<div class="products">
							<ul>
								<li>Amino Acids </li>
								<li>Animal Proteins </li>
								<li>Fats with Lecithin</li>
								<li>Fish By Products </li>
								<li>Lecithin </li>
							</ul>
						</div>
						<div class="products">
							<ul>
								<li>Milk Products</li>
								<li>Palm Fats (Powder)</li>
								<li>Phosphates </li>
								<li>Rape Fats (Powder)</li>
								<li>Rice (white & Brown) </li>
							</ul>
						</div>
						<div class="products">
							<ul>
								<li>Sugars, Starches and Fibres</li>
								<li>Spices</li>
								<li>Vegetable Proteins & Oils</li>
								<li>Wheat Products</li>
								<li>Yeast Products</li>
							</ul>
						</div>
					</div>
				</section>
			</div>
		</div>

		<div id="about-us">
			<div class="container">
				<div class="title">
					<img src="images/big-titles/about.png" />
				</div>
				<p>Established in 2002, Chase Trade Services is a family run business that provides the essential link between buyers and sellers of raw materials required by the animal feed and pet food industries across Europe and the UK.  We strive to provide the primary link in the supply of feed ingredients for farm animals and pets in an efficient and professional manner, ensuring that at the same time we offer a personalised service.</p>
				<p>Our philosophy remains the same: to continue to provide a premium service to our customers.</p>
			</div>
		</div>

		<div id="accolades-innovation">
			<div class="container">
				<div class="floating-block innovation">
					<div class="title">
						<img src="images/big-titles/innovation.png" />
					</div>
					<p class="intro">We keep our customers satisfied by making every effort to keep abreast of new and innovating products.</p>
					<p>There are now a number of products that are used regularly in the UK that were introduced by Chase Trade Services.</p>
					<p>At present, we have a number of new products that are being tested through either University studies or through field trials.  Due to recent successes many companies are willing to test products in order to help reduce existing costs, improve nutritional values or in some cases both.</p>
					<p>If you would like to know more about current products that are entering the UK, please contact us.</p>
				</div>
				<div class="floating-block accolades">
					<div class="title">
						<img src="images/big-titles/accolades.png"/>
					</div>
					<p class="intro">We are privileged to work with some of the best companies in the trade</p>
					<p>We endeavour to expand further to support their needs and the needs of new suppliers and customers.</p>
					<p>If requested, we would be happy to provide you with necessary references.</p>
					<p>“Working with Chase Trade Services is always a great pleasure. The highly experienced and qualified team have supported us for more than 10 years which reflects in our successful business relationship. We appreciate their constant support, their integrity as our agents and their extensive market activities“. C.Goedecke. Berg &amp; Schmidt</p>
				</div>
			</div>
		</div>

		<div id="contact-us">
			<div class="container">
				<a href="#top" class="back-to-top">
					<img src="images/back-to-top.png" alt="">
				</a>
				<div class="title">
					<img src="images/titles/lets-talk.png" />
				</div>
				<?php
				if($mailSent) {
					?>
					<div id="contact-response">
						<h3>Thank you</h3>
						<p>Thanks for your interest in the products and services provided by Chase Trade Services.  Your enquiry has been sent to our highly qualified team, one of whom will contact you shortly to discuss your requirements further.</p>
						<p>If your enquiry is urgent, please feel free to call us on <strong>01268 772096</strong>.</p>
					</div>
					<?php
				} else {
					?>
					<form action="" method="post" id="contact-form">
						<input type="hidden" name="submit-type" value="real-submission" />
						<input type="text" name="name" placeholder="Name" required />
						<input type="email" name="email" placeholder="Email" required />
						<input type="tel" name="tel" placeholder="Phone" />
						<textarea name="enquiry" placeholder="Write your message here." ></textarea>
						<button type="submit">send message</button>
					</form>
					<?php
				}
				?>
			</div>
		</div>

		<div id="footer">
			<div class="container">
				<div class="footer-block">
					<address>
						Chase Trade Services<br />
						Suite 1, Philpot House,<br />
						Station Road,<br />
						Rayleigh,<br />
						Essex,<br />
						SS6 7HH
					</address>
				</div>
				<div class="footer-block">
					<h4>Telephone:</h4>
					<p><span>Tel: +44 (0)1268 772404</span></p>
					<p><span>Fax: +44 (0)1268 772046</span></p>
				</div>
				<div class="footer-block">
					<h4>Email:</h4>
					<p><a href="mailto:graham@chasetradeservices.co.uk">graham@chasetradeservices.co.uk</a></p>
					<p><a href="mailto:neil@chasetradeservices.co.uk">neil@chasetradeservices.co.uk</a></p>
					<p><a href="mailto:michael@chasetradeservices.co.uk">michael@chasetradeservices.co.uk</a></p>
				</div>
				<div class="ufas">
					<img src="images/ufas.png" />
					<p>registration<br />number 900</p>
				</div>
			</div>
		</div>
		<div id="copyright">
			<div class="container">
				<p>Chase Trade Services &copy; <?php echo date('Y');?></p>
			</div>
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/core.js"></script>
</body>
</html>