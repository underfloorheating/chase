$(document).ready(function(){
	$('#mobile-nav-trigger').click(function(){
		$('#mobile-nav ul').toggleClass('open');
	});

	$('a[href^="#"]').click(function(){
		var id = $(this).attr('href');
		if(id !="#") {
			$('html,body').animate({
				scrollTop: $(id).offset().top
			},'swing');
		}
	});

	$('#contact-form').submit(function(e){
		var form = $(this);
		var name = $('input[name="name"]', form);
		var email = $('input[name="email"]', form);
		var tel = $('input[name="tel"]', form);
		var message = $('textarea', form);

		if(!validateEmail(email.val())) {
			alert('You didn\'t enter a valid email address');
			return false;
		}
		return true;
	});
});

//email validation
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}